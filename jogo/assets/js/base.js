/*jshint esversion: 6 */

// estruture de Classe base
let GameConfig = {
    GRAVITY     : 5,
    SPEED       : 5,
    MAX_LIFE    : 3,
    blocoDim    : {tw: 50, th:50},    
    mapa        :  null,
    win_w       : 800,
    win_h       : 600, 
    transMapa   : {x : 0, y: 0},
    cenariox    : 0,
    Objetos     : []
}

let player = null;

class ObjGame{

    constructor(x, y, w ,h, permeable){
        
        this.index = GameConfig.Objetos.length;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
        this.velocity = {x:0, y:0 }; //vetor do movimento
        this.inColision = false;
        this.name = 'ObjGame'
        this.permeable = permeable;  
        this.speed = 5;
        GameConfig.Objetos.push(this);
    }

    getSpeed(){
        return this.speed;
    }
    
    setSpeed(s){
        this.speed = s;
    }

    getIndex(){
        return this.index;
    }

    isPermeable(){
        return this.permeable;
    }

    setPermeable(p){
        this.permeable = p;
    }

    setName(n){
        this.name = n;
    }

    setVelocity(v){
        this.velocity = v;
    }

    getVelocity(){
        return this.velocity;
    }
    
    getName(){
        return this.name;
    }

    isInCollision(){
        return this.inColision;
    }

    setCollision(){
        this.inColision = true;
    }

    getX(){
        return this.x;
    }

    setX(x){
        this.x = x;
    }

    getY(){
        return this.y;
    }

    setY(y){
        this.y = y;
    }

    getWidth(){
        return this.width;
    }

    setWidth(w){
        this.width = w;
    }

    setHeight(h){
        this.height = h;
    }

    getHeight(){
        return this.height;
    }   

    draw(){
        rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    };

    isCollider(objGame){

        if(objGame == this || objGame == null)
            return;
        //collideRectRect(x, y, width, height, x2, y2, width2, height2 )
        this.inColision = collideRectRect(this.x - 1, this.y - 1, this.width + 1, this.height + 1,  
        objGame.getX(), objGame.getY(), objGame.getWidth(), objGame.getHeight());

        if(this.inColision){
            this.onCollision(objGame);
        }
      
        return this.inColision;

    }

    update(){
        this.draw();
    }

    //Carregar mídias 
    load(){}

    //o que fazer se colidir
    onCollision(objGame){}
  
}

class HUD extends ObjGame{

    constructor(x, y, player){
        super(x,y,350,80,true);  
        this.setName('Hud');
        this.player = player;
        GameConfig.Objetos[this.getIndex()] = null;
        this.jewels = '';
        
    }

    update(){
        this.setX(this.player.getX());
        this.spriteBox.position.x = this.getX();
    }

    load(){
        this.spriteBox = createSprite(this.getX() + 50,  20, this.getWidth(),this.getHeight());
        let f = 'assets/graphics/personagens/player/';
        this.spriteBox.addAnimation('idle', f + 'hud_idle.png');
        this.spriteBox.addAnimation('dano', f + 'hud_dano.png');  
        this.spriteBox.scale = 0.5;
        this.jewels = loadImage('assets/graphics/objetos/caju.png');

    }

    draw(){
        let dano = ((this.player.getLife()/GameConfig.MAX_LIFE));
        // fill(255,255,255);
        // rect(this.getX() - 50, this.getY(), this.getWidth(), this.getHeight());
        fill(255,0,0);
        rect(this.getX() + 50 , 10, 200, 20);
        fill(0,255,0);
        rect(this.getX() + 50, 10, 200 * dano , 20);       
        image(this.jewels, this.getX() + 70,  35,30, 50);
        fill(0);
        textSize(30);
        text('x ' + this.player.getQtdJewels() , this.getX() + 100, this.getY() + 80);
    };
}

class Tile extends ObjGame{

    constructor(permeable, x, y, w, h, scale, sprite, animated){
        super(x,y,w,h,permeable);  
        this.sprite = sprite;
        this.boxSprite = null;
        this.scale = scale;
        this.setName('Tile');
        this.animated = animated;
    }

    isAnimated(){

        return this.animated;
    }

    load(){
        if(this.animated){
            this.boxSprite = createSprite(this.getX(), this.getY(), this.getWidth(), this.getHeight());
            this.boxSprite.scale = scale
            let path = 'assets/graphics/tiles/' + this.sprite.path;
            this.boxSprite = loadSpriteSheet(path, + this.sprite.width, this.sprite.height, this.sprite.qtd);
        }
        else{
            this.boxSprite = loadImage(this.sprite);
        }            
    }

    draw(){

        if(!this.animated){
            image(this.boxSprite, this.getX(), this.getY(), this.getWidth(), this.getHeight());
        }       

    }
}

class TilePermeable extends Tile{

    constructor(x, y, w, h, scale, sprite, animated){
        super(true,x,y,w,h, scale, sprite, animated);
        this.setName('TilePemeable');
    }

}

class TileImpermeable extends Tile{

    constructor(x, y, w, h, scale, sprite, animated ){
        super(false,x,y,w,h, scale, sprite, animated);
        this.setName('TileIpermeable');
    }

}

class Trap extends Tile {

    constructor(permeable, x, y, w, h, scale, sprite, player, trigger){
        super(permeable,x,y,w,h, scale, sprite, true);  
        this.sprite = sprite;
        this.boxSprite = createSprite(x, y, w, h);
        this.boxSprite.scale = scale
        this.setName('Trap');
        this.active = false;
        this.target = player;
        this.trigger = trigger;
    }

    isActive(){

        if(
            ( (this.getX() - this.trigger)  <=  (this.target.getX() && this.target.getX() <= this.getX() + this.trigger)) || 
            ( (this.getY() - this.trigger)  <=  (this.target.getY() && this.target.getY() <= this.getY() + this.trigger))
        ){
            this.active = true;
            this.onActive();

        }else
        {
            this.toDesable();
        }
        

    }

    toDesable(){
        this.onDesable()
    }

    onActive(){}
    onDesable(){}
}

class Item extends Tile{

    constructor(x, y, w, h, scale, sprite){
        super(true,x,y,w,h, scale, sprite, false);
        this.setName('Item');
    }


}

class Plataforms extends TileImpermeable{

    constructor(x, y, type){
        let sprite = '';

        if(type == '#' ){
            sprite = 'assets/graphics/tiles/2.png';
        }
        else sprite = 'assets/graphics/tiles/5.png';

        super(x,y, GameConfig.blocoDim.tw, GameConfig.blocoDim.th, 0,sprite, false);
        this.setName('Plataform');
    }

}

class Decoration extends TilePermeable{
    constructor(x, y, w, h, sprite){
        super(x, y, w, h, 0, sprite, false);
        this.setName('Decoration');
    }

}

class FinalPoint extends TilePermeable{

    constructor(x, y, sprite){
        super(x, y, 50, 50, 0, sprite, false);
        this.setName('Decoration');
    }

}

const TypeItens = {

    WEAPON : 1,
    LIFE   : 2,
    AMMO   : 3,
    JEWEL  : 4

}

class ItemBonus extends Item{

    constructor(x, y, w, h, sprite, type, qtd){
        super(x, y, w, h, 0, sprite);
        this.setName('ItemBonus');
        this.type = type;
        this.qtd = qtd;
    }

    onCollision(objGame){

        if( objGame instanceof Player){

            switch(this.type){
                case TypeItens.LIFE  : 
                    if(objGame.getLife < GameConfig.MAX_LIFE){
                        for(let i = 0; i < this.qtd; i++){
                            objGame.incrementLife();
                        }                            
                    }
                     break;
                // case TypeItens.AMMO  : objGame.incrementAmmo(this.qtd); break;
                case TypeItens.JEWEL : objGame.incrementJewel(this.qtd); break;
            }

            GameConfig.Objetos[this.getIndex()] = null;
            
        }

    }

}

class Character extends ObjGame {

    constructor(x, y, w, h){
      super( x, y, w, h, false);
      this.life = 1;
      this.velocity = {x: 0 ,y: 1};
      this.salto = 5;
    }
   
    setLife(l){
      this.life = l;
    }

    getLife(){
        return this.life;
    }
  
    incrementLife(){
        this.life ++;
    }

    decrementLife(){
        this.life --;
        this.isDeath();
    }
      
  
    isDeath(){
        if(this.getLife <= 0){
            this.onDeath();
            return true;
        }
        else
            return false;
    }
  
    update(){
        super.update();
        this.toMove();
        
    }
  
    toMove(){
      this.onMove();
      this.setX(this.getX() + (this.getVelocity().x * this.getSpeed()));      
      this.setY(this.getY() + (this.getVelocity().y * GameConfig.GRAVITY));
    }
  
    toAttack(){
      this.onAttack();
    }
  
    toDeath(){
      this.onDeath();
  
    }
  
    toDefend(){
      this.onDefend();
    }
  
    toJump(){
        this.setY(this.getY() + this.getY());
    }
  
    onCollision(objGame){
      super.onCollision(objGame)
      console.log("Em colisao");
        // let v = {x:1,y:1};
      

    // //   ///colisao em x
    //   let xc = (
    //               ((this.getX() <= objGame.getX()) && (objGame.getX() <= this.getX() + this.getWidth() )) ||
    //               (this.getX()  <= objGame.getX() + objGame.getWidth()) && (objGame.getX() + objGame.getWidth() <= this.getX() + this.getWidth())
    //           ) ;

    //  console.log("xp: " + this.getX() + ", wp: " + (this.getX() + this.getWidth()) + " | ","xo: " + objGame.getX() + ", wo: " + (objGame.getX() +objGame.getWidth()) );
  
    // //   //colisao em y
    // //   let yc = (
    // //               ((this.getY()  <= objGame.getY()) && (objGame.getY()   <= this.getY() + this.getHeight() ))  ||
    // //               ((this.getY()  <= objGame.getY() + objGame.getHeight()) && (objGame.getY() + objGame.getHeight() <= this.getY() + this.getHeight()))
    // //            ) ;
      
        // if( objGame instanceof Tile){
            
        //     if(!objGame.isPermeable()){
        //         if
        //         if(xc){v.x = 0; console.log("colisoa em x")}  
                            
        //     }
        // }

        // this.setVelocity(v);
  
    }
  
    onJump(){}
    onAttack(){}
    onMove(){}
    onDeath(){}
    onDefend(){}
  }


  class Player extends Character {
  
    constructor(x, y, w, h){
        super(x, y, w, h);
        this.setSpeed(5);
        GameConfig.Objetos[this.getIndex()] = null;
        this.spriteBox = null;
        this.hud = new HUD(0,0,this);
        this.jewels = 0;
        this.setLife(GameConfig.MAX_LIFE);
    }

    getQtdJewels(){

        return this.jewels;
    }

    incrementJewel(qtd){

        return this.jewels += qtd;
    }

    decrementJewel(){

        return this.jewels --;
    }

    load(){
        this.hud.load();
        this.spriteBox = createSprite(this.getX(), this.getY(), this.getWidth(),this.getHeight());
        let f = 'assets/graphics/personagens/player/';
        this.spriteBox.addAnimation('idle', f + 'Idle__001.png', f + 'Idle__002.png',
        f + 'Idle__003.png', f + 'Idle__004.png', f + 'Idle__005.png', f + 'Idle__006.png',
        f + 'Idle__007.png',f + 'Idle__008.png',f + 'Idle__009.png');
    
        this.spriteBox.addAnimation('run', f + 'Run__001.png', f + 'Run__002.png',
        f + 'Run__003.png', f + 'Run__004.png', f + 'Run__005.png', f + 'Run__006.png',
        f + 'Run__007.png',f + 'Run__008.png',f + 'Run__009.png');
    
        this.spriteBox.addAnimation('jump', f + 'Jump__001.png', f + 'Jump__002.png',
        f + 'Jump__003.png', f + 'Jump__004.png', f + 'Jump__005.png', f + 'Jump__006.png',
        f + 'Jump__007.png',f + 'Jump__008.png',f + 'Jump__009.png');
    
        this.spriteBox.scale = 0.2;

    }
  
    update(){
        this.toMove() 
        this.spriteBox.position.x = this.getX() + 25;
        this.spriteBox.position.y = this.getY() + 50;
    }

    toMove(){
        this.onMove();

        let m = ((GameConfig.mapa.getWidth() - 1) * GameConfig.blocoDim.tw);
        let x = this.getX() + (this.getVelocity().x * this.getSpeed());
        let v = map(x, 50, m - 50, 0, 100);


        if(v < 0 || v >= 100){
            return;
        }

        this.setX(x);
        this.setY(this.getY() + (this.getVelocity().y * GameConfig.GRAVITY));

        for(let i = 0; i < GameConfig.Objetos.length - 1; i++){
            if(this.isCollider(GameConfig.Objetos[i])){
                break;
            }            
        } 
        
        if(this.isInCollision()){
            let vf = {x: this.getVelocity().x  , y: this.getVelocity().y * -1};
            
            this.setVelocity(vf);
            this.setX(this.getX() + (this.getVelocity().x * this.getSpeed()));
            this.setY(this.getY() + (this.getVelocity().y * GameConfig.GRAVITY));
        }
        
        if(v > 0 && v <= 90){
            GameConfig.transMapa.x = this.getX() - 50 ;
            this.hud.update();
        } 

      }

  
    onMove(){
      let v = {x:0 , y:1};
      let idle = true;
    
      if(keyIsDown(RIGHT_ARROW) ){

        v.x = 1;     
        this.spriteBox.changeAnimation('run');
        this.spriteBox.mirrorX(1);
        idle = false;
      }
  
      if(keyIsDown(LEFT_ARROW)){    
        v.x = -1;
        this.spriteBox.changeAnimation('run');
        this.spriteBox.mirrorX(-1);
        idle = false;
      }

      if(keyWentDown(UP_ARROW)){
        this.toJump();
        //this.animation.changeAnimation('jump');
        
      }

      if(idle){
        this.spriteBox.changeAnimation('idle');
      }

      this.setVelocity(v);

    }

    draw(){
        this.hud.draw();
        
        // fill(255, 0, 0);
        // noStroke();
        // rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }

    onCollision(objGame){
        super.onCollision(objGame);

        if(objGame instanceof ItemBonus){
            objGame.onCollision(this);
        }

        let v = this.getVelocity();
        let x = this.getX() - objGame.getX();

        if(x < 0){
            v.x
        }
    }
  
  }
  
class Mapa {

    constructor(){
      this.bg = loadImage('assets/graphics/backgrouds/BG.png');
      this.xp = 0;
      this.yp = 0;
      this.matriz = [ 
                      "",
                      "",          
                      "",          
                      "",          
                      "",          
                      "..................................................................MMM...........MMMM...............................................",
                      "..................................................................###....M......####...............................................",
                      "........................................................................###....................D...r...D...e........D........E....P",
                      "............................................................e......B..........e...D..A...g..#######################################",
                      ".......................................................#####################################***************************************",
                      "p..M.M.M..#..D....DDAD...G.D.R..D..DDBD..J.D..e..D....#****************************************************************************",
                      "######################################################*****************************************************************************"
                    ]; 
  
  
      
      for(let l = 0; l < this.matriz.length; l++){
        for(let c = 0; c < this.matriz[l].length; c++){
          let x = c*GameConfig.blocoDim.tw;
          let y = l*GameConfig.blocoDim.th; 
          this.createTile(x,y,this.matriz[l][c]);
        }
      }


    }

    getBG(){
      return this.bg;
    }

    getWidth(){
        return this.matriz[this.matriz.length - 1].length;
    }

    getHeight(){
        return this.matriz.length;
    }
  
    createTile(x,y,tipo){
      let obj = null;
      let filetx  = null;
      let v = 0
      let objs = "CXAaGgRrB";
    
      let f = tipo;
      if(f == "D") {
        v = Math.floor((Math.random(new Date().getMilliseconds()) * objs.length));
        f = objs[v];
      }

      switch(f){
        case  "#":    
          obj = new Plataforms(x, y,f);          
          break;
        case  "*":    
          obj = new Plataforms(x, y,f);          
          break;
        case  "C": 
            v = Math.floor((Math.random() * 3) + 1);
            filetx = 'assets/graphics/objetos/cacto' + v + '.png';
            obj = new Decoration(x, y - 20,70,70,filetx);            
          break;
        case  "X": 
          filetx = 'assets/graphics/objetos/caixa.png';
          obj = new Decoration(x, y,50,50,filetx);   
        break;
        case  "P": 
          filetx = 'assets/graphics/objetos/placa1.png';
          obj = new FinalPoint(x, y,filetx);   
        break;
        case  "p": 
          filetx = 'assets/graphics/objetos/placa2.png';
          obj = new Decoration(x, y,50,50,filetx);
        break;
        case  "A": 
          filetx = 'assets/graphics/objetos/arvore1.png';
          obj = new Decoration(x, y - 100,150,150,filetx);
        break;
        case  "a": 
            v = Math.floor((Math.random() * 2) + 1);
            filetx = 'assets/graphics/objetos/arbusto' + v + '.png';
            obj = new Decoration(x, y - 30,80,80,filetx);
          break;
        case  "G": 
            v = Math.floor((Math.random() * 2) + 1);
            filetx = 'assets/graphics/objetos/grama' + v + '.png';  
            obj = new Decoration(x, y - 30,80,80,filetx);
        break;
        case  "g": 
            v = Math.floor((Math.random() * 2) + 1);
            filetx = 'assets/graphics/objetos/grama' + v + '.png';  
            obj = new Decoration(x, y,50,50,filetx);
        break;
        case  "R": 
          filetx = 'assets/graphics/objetos/pedra1.png';
          obj = new Decoration(x, y-100,150,150,filetx);
        break;
        case  "r": 
        filetx = 'assets/graphics/objetos/pedra1.png';
        obj = new Decoration(x, y + 10,40,40,filetx);
        break;
        case  "B": 
          filetx = 'assets/graphics/objetos/boi.png';
          obj = new Decoration(x, y + 20,60,30,filetx);
        break;
        case  "M": 
          filetx = 'assets/graphics/objetos/caju.png';
          obj = new ItemBonus(x, y - 10,30,50,filetx, TypeItens.JEWEL, 1);
        break;
        case  "J": 
            this.xp = x;
            this.yp = y - 50;
          break;
      }
      
    }
}

player = new Player(55, 100, 48, 90);

let x = 0;

function preload(){ 

    GameConfig.mapa = new Mapa();
    for(let i  = 0; i < GameConfig.Objetos.length; i++){
       if(GameConfig.Objetos[i]){
            GameConfig.Objetos[i].load();
       }
    }    


    player.load();
} 

function setup() {
    createCanvas(GameConfig.win_w, GameConfig.win_h); 
}

function draw() {
    background(GameConfig.mapa.getBG());
    
    player.update();

    translate( -GameConfig.transMapa.x, GameConfig.transMapa.y, 0);
    
    for(let i  = 0; i < GameConfig.Objetos.length; i++){
        if((GameConfig.Objetos[i]) && GameConfig.Objetos[i] != player ) {
          GameConfig.Objetos[i].update();
        } 
    } 

    player.draw();
        
    //draw the sprite
    drawSprites(); 
}
